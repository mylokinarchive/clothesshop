var xPos = 0;
var yPos = 0;
var timeout = new Date().getTime();
$(document).ready(function(){
		      $("#slideshow").css("margin", "0");
		      $("#gallery").galleryView({
						    panel_width: $("#slideshow").width(),
						    panel_height: $(window).height()-$("#category").height()-$('#header').height()-30,
						    show_filmstrip: false,
						    frame_width: 80,
						    frame_height: 40,
						    border: "none",
						    pause_on_hover: true,
						    transition_interval: 5000,
						    background_color: "black",
						    nav_theme: "light",
						    panel_scale: "crop",
						});
		      $(".panel").hover(
			  function(){
			      $(this).find(".panel-overlay").fadeIn();
			      $(this).find(".overlay-background").fadeIn();    
			  },
			  function(){
			      $(this).find(".panel-overlay").fadeOut();
			      $(this).find(".overlay-background").fadeOut();    
			  }
		      );
		  });
