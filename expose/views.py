from django.db.models import Count
from django.http import Http404
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext

import interface as I

def index(request):
    sex = request.session.get('sex', I.SEX)
    articles = I.Article.random(3, sex)
    categories = I.Category.all(sex)
    context = RequestContext(request, locals())
    return render_to_response('index.html', context)

def category(request, category):
    sex = request.session.get('sex', I.SEX)
    current_category = category = I.Category.get(category)
    try:
        articles = I.Article.category(category, sex)
    except Http404:
        return redirect('/')
    #Chose trademark if only 1 trademark
    if len(articles.values('trademark').annotate(Count('trademark')))==1:
        return redirect('expose:trademark', category=category, \
            trademark=articles[0].trademark)
    categories = I.Category.all(sex)
    trademarks = I.Trademark.category(category, sex)
    context = RequestContext(request, locals())
    return render_to_response('category.html', context)
    
def trademark(request, category, trademark):
    sex = request.session.get('sex', I.SEX)
    current_category = category = I.Category.get(category)
    if request.GET.has_key('disable'):
        return redirect('expose:category', category=category)
    current_trademark = trademark = I.Trademark.get(trademark)
    try:
        articles = I.Article.category_n_trademark(category, trademark, sex)
    except Http404:
        return redirect('expose:category', category=category)
    if articles.count()==1:
        return redirect('expose:article', article=articles[0].id)
    categories = I.Category.all(sex)
    trademarks = I.Trademark.category(category, sex)
    context = RequestContext(request, locals())
    return render_to_response('trademark.html', context)

def sex(request, sex):
    request.session['sex'] = sex
    return redirect(request.META.get('HTTP_REFERER', '/'))

def article(request, article):
    sex = request.session.get('sex', I.SEX)
    article = I.Article.get(article)
    current_category = article.category
    current_trademark = article.trademark
    if sex!=article.sex:
        return redirect('expose:trademark', category=current_category, \
            trademark=current_trademark)
    photos = article.photo_set.all()
    categories = I.Category.all(sex)
    context = RequestContext(request, locals())
    return render_to_response('article.html', context)

