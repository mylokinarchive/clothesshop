from django.conf.urls.defaults import *

urlpatterns = patterns('expose.views',
    url(r'^$', 'index', name='index'), 
    url(r'^sex/(?P<sex>m|f|u)/$', 'sex', name='sex'),
    url(r'^article/(?P<article>\d+)/$', 'article', name='article'),
    url(r'^(?P<category>\w+)/$', 'category', name='category'),
    url(r'^(?P<category>\w+)/(?P<trademark>\w+)/$', 'trademark', name='trademark'),
)
