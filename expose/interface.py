from django.db.models import Count
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404

import models

SEX = [sex for sex, designation in models.SEX]

class Interface(object):
    manager = None
    
    @classmethod
    def all(cls):
        return cls.manager.all()

    @classmethod
    def random(cls, number):
        return cls.manager.order_by('?')[:number]


class Trademark(Interface):
    manager = models.Trademark.objects

    @classmethod
    def category(cls, category, sex=SEX):
        return cls.manager.annotate(photos=Count('article__photo')).\
            filter(article__category=category, article__sex__in=sex, photos__gte=1)

    @classmethod
    def get(cls, trademark):
        return get_object_or_404(models.Trademark, name=trademark.title())
    
class Article(Interface):
    manager = models.Article.objects

    @classmethod
    def category(cls, category, sex=SEX):
        query_set = cls.manager.annotate(photos=Count('photo')).filter(category=category, \
            sex__in=sex, photos__gte=1)
        if query_set.count():
            return query_set
        else:
            raise Http404

    @classmethod
    def category_n_trademark(cls, category, trademark, sex=SEX):
        query_set = cls.manager.annotate(photos=Count('photo')).filter(category=category, \
            trademark=trademark, sex__in=sex, photos__gte=1)
        if query_set.count():
            return query_set
        else:
            raise Http404

    @classmethod
    def random(cls, number, sex=SEX):
        return cls.manager.annotate(photos=Count('photo')).filter(sex__in=sex, \
            photos__gte=1).order_by('?')[:number]

    @classmethod
    def get(cls, article):
        return get_object_or_404(models.Article, id=article)


class Photo(Interface):
    manager = models.Photo.objects

class Category(Interface):
    manager = models.Category.objects

    @classmethod
    def all(cls, sex=SEX):
        return cls.manager.annotate(photos=Count('article__photo'),\
            articles=Count('article')).\
            filter(articles__gte=1, article__sex__in=sex, photos__gte=1)
    
    @classmethod
    def get(cls, category):
        return get_object_or_404(models.Category, name=category)

