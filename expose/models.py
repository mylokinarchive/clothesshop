from django.contrib import admin
from django.db import models

SEX = (
    (u'm', u'Male'),
    (u'f', u'Female'),
)

class Article(models.Model):
    category = models.ForeignKey('Category')
    description = models.TextField(null=True, blank=True)
    price = models.IntegerField()
    sex = models.CharField(max_length=1, choices=SEX)
    title = models.CharField(max_length=200, null=True, blank=True)
    trademark = models.ForeignKey('Trademark')

    def __unicode__(self):
        return self.title

    def random_photo(self):
        return self.photo_set.order_by('?')[0]

class Category(models.Model):
    name = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.name

class Photo(models.Model):
    article = models.ForeignKey('Article')
    image = models.ImageField(upload_to='photos')
    
    def __unicode__(self):
        return u"Photo: %s"%self.article
    
class Trademark(models.Model):
    name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='trademarks')
    
    def __unicode__(self):
        return self.name

class PhotoInline(admin.TabularInline):
    model = Photo

class ArticleAdmin(admin.ModelAdmin):
    inlines = [
        PhotoInline,
    ]        

admin.site.register(Article, ArticleAdmin)
admin.site.register(Category)
admin.site.register(Trademark)
