from django.conf.urls.defaults import *
from django.contrib import admin
import settings

admin.autodiscover()


urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    (r'^', include('shop.expose.urls', namespace='expose', app_name='expose')),
)

if settings.DEBUG:
    urlpatterns += patterns('django.views',
        url(r'^'+settings.MEDIA_URL[1:]+'(.*)$',
            'static.serve',
            dict(document_root=settings.MEDIA_ROOT)
        ),
    )
